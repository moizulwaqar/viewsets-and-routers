from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
# Create your models here.


class User(AbstractUser):
    phone_number = models.CharField(
        max_length=16,
        blank=True,
        null=True,
        validators=[
            RegexValidator(
                regex=r'^\+?1?\d{9,15}$',
                message="Phone number must be entered in the format '+123456789'. Up to 15 digits allowed."
            ),
        ],
    )
    birth_date = models.DateField(null=True, blank=True)
    google_id = models.CharField(max_length=50, null=True, blank=True)
    facebook_id = models.CharField(max_length=50, null=True, blank=True)
    REQUIRED_FIELDS = []
