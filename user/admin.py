from django.contrib import admin
# from django.contrib.auth.admin import UserAdmin
from .models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'email', 'first_name', 'last_name')
    search_fields = ['username', 'email', 'first_name', 'last_name', ]

    def get_queryset(self, request):
        # qs = super(CertificationSetting, self).get_queryset(request)
        # if request.user.is_superuser:
        #     return qs
        return User.objects.all()


admin.site.register(User, UserAdmin)
